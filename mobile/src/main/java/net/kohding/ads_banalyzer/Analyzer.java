package net.kohding.ads_banalyzer;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.koushikdutta.async.http.AsyncHttpClient;
import com.koushikdutta.async.http.WebSocket;

import java.io.IOException;


public class Analyzer extends Service implements StratuxClient{

    private final static String LOG_TAG = "ADSB-Analyzer";
    private final TrafficDatabase trafficDatabase = MainActivity.getTrafficDatabase();
    private static Situation situation = new Situation();
    private NotificationManager notificationManager;
    private NotificationCompat.Builder notificationBuilder;

    @Override
    public void onCreate() {

        //TODO Add foreground consistent notification
        notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, new Intent(this, MainActivity.class), 0);
        notificationBuilder = new NotificationCompat.Builder(this)
                                .setContentTitle("Threat Alert")
                                .setSmallIcon(R.drawable.ic_notification)
                                .setContentIntent(pendingIntent);

        Log.d(LOG_TAG, "Service Created");

        /*  TODO Change the WebSockets to use https://github.com/TakahikoKawasaki/nv-websocket-client instead
        *   The AndroidAsync library doesn't handle closing of the Websocket according to RFC6455
        */
        AsyncHttpClient.getDefaultInstance().websocket(WEBSOCKET_HOST + "/traffic", "ws", new AsyncHttpClient.WebSocketConnectCallback() {
            @Override
            public void onCompleted(Exception ex, WebSocket webSocket) {
                if (ex != null){
                    //TODO Retry to connect to WS
                    ex.printStackTrace();
                    return;
                }
                Log.i(LOG_TAG, "Traffic WS Connected");
                webSocket.setStringCallback(new Analyzer.Callback("traffic"));
            }
        });

        AsyncHttpClient.getDefaultInstance().websocket(WEBSOCKET_HOST + "/situation", "ws", new AsyncHttpClient.WebSocketConnectCallback() {
            @Override
            public void onCompleted(Exception ex, WebSocket webSocket) {
                if (ex != null){
                    ex.printStackTrace();
                    return;
                }
                Log.i(LOG_TAG, "Situation WS Connected");
                webSocket.setStringCallback(new Analyzer.Callback("situation"));
            }
        });

        super.onCreate();
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if(intent.hasExtra("traffic")){
            Log.i(LOG_TAG, "Parsing Traffic Intent");
            updateTraffic(intent.getStringExtra("traffic"));
        } else if(intent.hasExtra("situation")){
            Log.i(LOG_TAG, "Parsing Situation Intent");
            updateSituation(intent.getStringExtra("situation"));
        }

        return START_NOT_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    private class Callback implements WebSocket.StringCallback {
        private String type;

        private Callback(String type){
            super();
            this.type = type;
        }

        @Override
        public void onStringAvailable(String s) {
            Log.i(LOG_TAG, type + "received");
//            Log.d("ADSB-WS", s);
            Intent intent = new Intent(Analyzer.this, Analyzer.class);
            intent.putExtra(type, s);
            startService(intent);
        }
    }

    private class ThreatPredictor extends AsyncTask<TrafficUpdate, Void, Void> {
        TrafficUpdate trafficUpdate;

        @Override
        protected Void doInBackground(TrafficUpdate... trafficUpdates) {
            trafficUpdate = trafficUpdates[0];

            // This would mean we're not moving, which means we're not flying.
            if(situation.GroundSpeed != 0){
                trafficUpdate.isThreat = checkSituation(situation, trafficUpdate);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            // ignore updates that don't have valid data
            if(trafficUpdate.Position_valid && trafficUpdate.Speed_valid) {
                trafficDatabase.addTraffic(trafficUpdate);
                int id = TrafficDatabase.trafficMap.indexOf(trafficUpdate);
                if (trafficUpdate.isThreat) {
                    notificationBuilder.setContentText(trafficUpdate.Reg + " may be a threat.");
                    Notification notification = notificationBuilder.build();
                    notification.flags = Notification.DEFAULT_VIBRATE | Notification.FLAG_ONLY_ALERT_ONCE | Notification.PRIORITY_HIGH;
                    notificationManager.notify(id, notification);
                } else {
                    notificationManager.cancel(id);
                }
            }
            super.onPostExecute(aVoid);
        }
    }

    private synchronized boolean checkSituation(Situation s, TrafficUpdate t){
        boolean alert = false;
        double f1 = 0.0;
        double f2 = 0.0;

        for(double time = 1.0; time < 10.0; time++){
            f1 = ((double)s.GroundSpeed/60.0 * time * Math.sin(s.TrueCourse)) -
                    (Math.sin(Math.toRadians(t.Bearing)) + (double)t.Speed/60.0 * time * Math.sin(Math.toRadians(t.Track)));
            f2 = (s.GroundSpeed/60 * time * Math.cos(s.TrueCourse)) -
                    (Math.cos(Math.toRadians(t.Bearing)) + t.Speed/60 * time * Math.cos(Math.toRadians(t.Track)));
//            Log.d(LOG_TAG, "F1: " +f1+" F2: "+f2+ " T: " + time);
            alert = f1 < 2 && f2 < 2;
            if(alert){ break; }
        }

        return alert;
    }

    private void updateTraffic(String s){
        try {
            TrafficUpdate parsedUpdate = TRAFFIC_JSON_ADAPTER.fromJson(s);
            Log.d(LOG_TAG, "Traffic Update Parsed\n" + parsedUpdate);
            new ThreatPredictor().execute(parsedUpdate);
        } catch (IOException e) {
            Log.e(LOG_TAG, "Something is wrong with Traffic JSON from Stratux");
            e.printStackTrace();
        }
    }

    private void updateSituation(String s){
        try{
            situation = SITUATION_JSON_ADAPTER.fromJson(s);
//            Log.d(LOG_TAG, "Situation Update Parsed\n" + situation);
        } catch (IOException e) {
            Log.e(LOG_TAG, "Something is wrong with Situation JSON from Stratux");
            e.printStackTrace();
        }
    }

    public static synchronized Situation getSituation(){
        return situation;
    }
}
