package net.kohding.ads_banalyzer;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SimpleItemAnimator;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

public class MainActivity extends AppCompatActivity {

    private static final String LOG_TAG = "ADSB_Main";

    private static TrafficDatabase trafficDatabase;
    private RecyclerView recyclerView;
    private SituationHandler situationHandler;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loader);

        // Hide the actionbar until the service is started
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }

        setContentView(R.layout.activity_main);
        if (actionBar != null && !actionBar.isShowing()) {
            actionBar.show();
        }

        recyclerView = (RecyclerView) findViewById(R.id.traffic_view);
        recyclerView.setAdapter(getTrafficDatabase().getAdapter());
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        //This enabled recyclerView to run a little smoother
        recyclerView.setHasFixedSize(true);
        //Disable to notifyItemChanged animation
        ((SimpleItemAnimator) recyclerView.getItemAnimator()).setSupportsChangeAnimations(false);

        situationHandler = new SituationHandler((GridLayout) findViewById(R.id.situation_grid));

        startService(new Intent(this, Analyzer.class));
    }

    @Override
    protected void onDestroy() {
        Intent stopAnalyzerIntent = new Intent(this, Analyzer.class);
        stopService(stopAnalyzerIntent);
        recyclerView.setAdapter(null);
        super.onDestroy();
    }

    @Override
    protected void onStart() {
        situationHandler.start();
        super.onStart();
    }

    @Override
    protected void onStop() {
        situationHandler.stop();
        super.onStop();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            Intent startSettingsActivity = new Intent(this, SettingsActivity.class);
            startActivity(startSettingsActivity);
            return true;
        }
        if (id == R.id.action_web_ui) {
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://192.168.10.1"));
            if (intent.resolveActivity(getPackageManager()) != null) {
                startActivity(intent);
            }
        }
        return super.onOptionsItemSelected(item);
    }

    public static TrafficDatabase getTrafficDatabase() {
        if (trafficDatabase == null){
            trafficDatabase = new TrafficDatabase();
        }
        return trafficDatabase;
    }
}
