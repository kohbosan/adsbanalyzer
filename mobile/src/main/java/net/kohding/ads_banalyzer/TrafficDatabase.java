package net.kohding.ads_banalyzer;

import android.content.Context;
import android.graphics.Color;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.ListIterator;
import java.util.Locale;

public class TrafficDatabase {

    private final String LOG_TAG = "ADSB_DB";
    private final String ALTITUDE_FORMAT = "Alt: %6d ft";
    private final String GROUND_SPEED_FORMAT = "Ground Speed: %6d kt";
    private final String BEARING_FORMAT = "Bearing: %6.2f°";
    private final String DISTANCE_FORMAT = "Distance: %6.2s NM";

    final static ArrayList<TrafficUpdate> trafficMap = new ArrayList<>();
    private TrafficDatabaseAdapter trafficAdapter;
    private TrafficPurger trafficPurger;

    TrafficDatabase() {
        trafficAdapter = new TrafficDatabaseAdapter();
        trafficPurger = new TrafficPurger();
    }

    class TrafficDatabaseAdapter extends RecyclerView.Adapter<TrafficDatabase.TrafficViewHolder> {
        @Override
        public TrafficViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//            Log.d(LOG_TAG, "onCreateViewHolder Called");
            Context context = parent.getContext();
            LayoutInflater inflater = LayoutInflater.from(context);
            View trafficView = inflater.inflate(R.layout.item_traffic, parent, false);

            trafficPurger.start();

            return new TrafficViewHolder(trafficView);
        }

        @Override
        public void onBindViewHolder(TrafficViewHolder holder, int position) {
//            Log.d(LOG_TAG, "onBindViewHolder Called: Position is " + position);
            TrafficUpdate trafficUpdate = trafficMap.get(position);

            holder.layout
                    .setBackgroundColor(Color.parseColor(trafficUpdate.isThreat ? "#ee0000" : "#eeeeee"));
            holder.registration.setText(trafficUpdate.Reg);
            holder.altitude.setText(String.format(Locale.US, ALTITUDE_FORMAT, trafficUpdate.Alt));
            holder.ground_speed.setText(String.format(Locale.US, GROUND_SPEED_FORMAT, trafficUpdate.Speed));
            holder.bearing.setText(String.format(Locale.US, BEARING_FORMAT, trafficUpdate.Bearing));
            holder.distance.setText(String.format(Locale.US, DISTANCE_FORMAT, trafficUpdate.Distance));
        }

        @Override
        public void onDetachedFromRecyclerView(RecyclerView recyclerView) {
            super.onDetachedFromRecyclerView(recyclerView);
            trafficPurger.stop();
        }

        @Override
        public int getItemCount() {
//            Log.d(LOG_TAG, "getItemCount Called: Size is " +  trafficMap.size());
            return trafficMap.size();
        }
    }

    public TrafficDatabaseAdapter getAdapter() {
        if (trafficAdapter == null) {
            trafficAdapter = new TrafficDatabaseAdapter();
        }
        return trafficAdapter;
    }

    class TrafficPurger {

        Handler handler;
        DateFormat df;

        TrafficPurger() {
            handler = new Handler(Looper.getMainLooper());
            df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.US);
        }

        Runnable purgerThread = new Runnable() {
            @Override
            public void run() {
                try {
                    checkAge();
                } finally {
                    handler.postDelayed(purgerThread, 10000);
                }
            }
        };

        public synchronized void checkAge() {
            long checkTime = new Date().getTime();
            long itemAge = 0;
            boolean changeFlag = false;

            // This is very slow. There must be a better way!
            for (ListIterator<TrafficUpdate> iter = trafficMap.listIterator(); iter.hasNext(); ) {
                TrafficUpdate item = iter.next();
                try {
                    itemAge = checkTime - df.parse(item.Timestamp).getTime();
                    Log.d(LOG_TAG, item.Reg + " age: " + checkTime + " - " + df.parse(item.Timestamp).getTime() + " = " + itemAge);
                } catch (ParseException e) {
                    Log.e(LOG_TAG, "Error parsing Timestamp");
                    e.printStackTrace();
                }

                // While we're here, look for items older than some time
                //COMPLETE Check for age > 5 secs
                //TODO Get this value from user preferences (Partially done; have to restart app)
                if (Math.abs(itemAge) > 5000) {
//                    iter.remove();
                    changeFlag = true;
//                    MainActivity.getTrafficDatabase().getAdapter().notifyItemRemoved(iter.nextIndex() - 1);
                }
            }
//            if (changeFlag) { MainActivity.getTrafficDatabase().getAdapter().notifyDataSetChanged(); }
        }

        public void start() {
            handler.postDelayed(purgerThread, 10000);
        }

        public void stop() {
            handler.removeCallbacks(purgerThread);
        }
    }

    synchronized void addTraffic(TrafficUpdate trafficUpdate) {
        Log.i(LOG_TAG, "Handling Traffic Update");
//        //Timestamp Format is 2016-09-07T17:40:18.23Z
//        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.US);
//
//        long checkTime = new Date().getTime();
//        long itemAge = 0;


        // This is very slow. There must be a better way!
        for (ListIterator<TrafficUpdate> iter = trafficMap.listIterator(); iter.hasNext(); ) {
            TrafficUpdate item = iter.next();
//            try {
//                itemAge = checkTime-df.parse(item.Timestamp).getTime();
//                Log.d(LOG_TAG, item.Reg + " age: " + checkTime + " - " + df.parse(item.Timestamp).getTime() + " = " + itemAge);
//            } catch (ParseException e) {
//                Log.e(LOG_TAG, "Error parsing Timestamp");
//                e.printStackTrace();
//            }

            // Traffic already in list, so update
            if (item.Reg.equals(trafficUpdate.Reg)) {
                iter.set(trafficUpdate);
                this.getAdapter().notifyItemChanged(iter.nextIndex() - 1);
                Log.i(LOG_TAG, "Traffic Updated\n" + trafficMap.toString());
                return;
            }

            // While we're here, look for items older than some time
            //COMPLETE Check for age > 5 secs
            //TODO Get this value from user preferences (Partially done; have to restart app)
//            if(Math.abs(itemAge) > 5000){
//                iter.remove();
//                this.getAdapter().notifyItemRemoved(iter.nextIndex()-1);
//            }

        }

        // Traffic not found in list, so add it
        trafficMap.add(0, trafficUpdate);
        this.getAdapter().notifyItemInserted(0);

        Log.i(LOG_TAG, "Traffic Added\n" + trafficMap.toString());
    }

    public class TrafficViewHolder extends RecyclerView.ViewHolder {
        LinearLayout layout;
        TextView registration;
        TextView altitude;
        TextView ground_speed;
        TextView bearing;
        TextView distance;

        TrafficViewHolder(View itemView) {
            super(itemView);
            layout = (LinearLayout) itemView.findViewById(R.id.item_layout);
            registration = (TextView) itemView.findViewById(R.id.item_registration);
            altitude = (TextView) itemView.findViewById(R.id.item_altitude);
            ground_speed = (TextView) itemView.findViewById(R.id.item_gs);
            bearing = (TextView) itemView.findViewById(R.id.item_bearing);
            distance = (TextView) itemView.findViewById(R.id.item_distance);
        }
    }
}
