package net.kohding.ads_banalyzer;

class Situation {
    float LastFixSinceMidnightUTC;
    float Lat;
    float Lng;
    short Quality;
    float HeightAboveEllipsoid;
    float GeoidSep;
    int Sattelites;
    int SatellitesTracked;
    float Accuracy;
    short NACp;
    float Alt;
    float AccuracyVert;
    float GPSVertVel;
    String LastFixLocalTime;
    float TrueCourse;
    short GroundSpeed;
    String LastGroundTrackTime;
    String GPSTime;
    String LastGPSTimeTime;

//    double Track;

    public String toString(){
        return String.format("Alt: %.2f Heading: %.2f GS: %d Loc: %.2f, %.2f Acc: %.2f", Alt, TrueCourse, GroundSpeed, Lat, Lng, Accuracy);
    }
}
