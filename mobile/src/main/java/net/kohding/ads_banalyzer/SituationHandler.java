package net.kohding.ads_banalyzer;

import android.os.Handler;
import android.os.Looper;
import android.support.v7.widget.GridLayout;
import android.util.Log;
import android.widget.TextView;

class SituationHandler {
    private final String LOG_TAG = "ADSB_SitH";
    private final String ALTITUDE_FORMAT = "%.0f ft";
    private final String HEADING_FORMAT = "%.1f°";
    private final String GS_FORMAT = "%d kt";
    private final String LOCATION_FORMAT = "%s,%s";
    private final String ACCURACY_FORMAT = "±%.1f";

    private Handler handler;
    private GridLayout situation_grid;
    private Situation currentSituation;

    SituationHandler(GridLayout situation_grid) {
        this.situation_grid = situation_grid;
        handler = new Handler(Looper.getMainLooper());
    }

    Runnable situationUpdater = new Runnable() {
        @Override
        public void run() {
            try {
                updateViews();
            } finally {
                handler.postDelayed(situationUpdater, 2000);
            }
        }
    };

    private void updateViews(){
        currentSituation = Analyzer.getSituation();
        Log.d(LOG_TAG, currentSituation.toString());
        ((TextView) situation_grid.findViewById(R.id.sit_alt)).setText(String.format(ALTITUDE_FORMAT, currentSituation.Alt));
        ((TextView) situation_grid.findViewById(R.id.sit_hdg)).setText(String.format(HEADING_FORMAT, currentSituation.TrueCourse));
        ((TextView) situation_grid.findViewById(R.id.sit_gs)).setText(String.format(GS_FORMAT, currentSituation.GroundSpeed));
        ((TextView) situation_grid.findViewById(R.id.sit_loc)).setText(String.format(LOCATION_FORMAT, currentSituation.Lat, currentSituation.Lng));
        ((TextView) situation_grid.findViewById(R.id.sit_acc)).setText(String.format(ACCURACY_FORMAT, currentSituation.Accuracy));
    }

    void start() {
        handler.postDelayed(situationUpdater, 1000);
    }

    void stop() {
        handler.removeCallbacks(situationUpdater);
    }
}
