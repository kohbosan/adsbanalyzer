package net.kohding.ads_banalyzer;

class TrafficUpdate {
    boolean ExtrapolatedPosition;
    int Vvel;
    String Lat;
    String Squawk;
    double Distance;
    String Tail;
    boolean OnGround;
    short NACp;
    short Last_source;
    short TargetType;
    short GnssDiffFromBaroAlt;
    float SignalLevel;
    long Icao_addr;
    boolean Speed_valid;
    String Lng;
    short NIC;
    int Alt;
    double Bearing;
    boolean Position_valid;
    String Last_GnssDiff;
    String Timestamp;
    float AgeLastAlt;
    boolean AltIsGNSS;
    String Last_speed;
    short Emitter_category;
    float Age;
    short Addr_type;
    int Last_GnssDiffAlt;
    String Last_alt;
    short Track;
    String Reg;
    String Last_seen;
    short Speed;
    boolean isThreat;

    public String toString(){
        return this.Reg + ' ' + this.Timestamp + ' ' + this.isThreat + ' ' + this.Distance + ' ' + this.Bearing + ' ' + Speed_valid + ' ' + Position_valid;
    }
}
