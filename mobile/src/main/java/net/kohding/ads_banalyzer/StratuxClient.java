package net.kohding.ads_banalyzer;


import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;

interface StratuxClient {
    String WEBSOCKET_HOST = "ws://192.168.10.1";
//    String WEBSOCKET_HOST = "ws://10.0.2.2:8000";
    JsonAdapter<TrafficUpdate> TRAFFIC_JSON_ADAPTER =  new Moshi.Builder().build().adapter(TrafficUpdate.class);
    JsonAdapter<Situation> SITUATION_JSON_ADAPTER = new Moshi.Builder().build().adapter(Situation.class);
}
